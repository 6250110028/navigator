import 'package:flutter/material.dart';
import 'package:navigator/welcome_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Pass and Return Value',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Passing Value'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController _name = TextEditingController();
  TextEditingController _email = TextEditingController();
  TextEditingController _phone = TextEditingController();

  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Passing Value'),
        backgroundColor: Colors.green,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child:  TextField(
                controller: _name,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),labelText: 'Enter your Name'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child:  TextField(
                controller: _email,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),labelText: 'Enter your Email'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child:  TextField(
                controller: _phone,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),labelText: 'Enter your Phone Number'),
              ),
            ),
            ElevatedButton(
                onPressed: () async {
                  _incrementCounter();
                  final result = await Navigator.of(context).push(
                      MaterialPageRoute(
                          builder: (context) => WelcomePage(
                              name: _name.text,
                              email:_email.text,
                              phone: _phone.text)));
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content:Text('$result, press button : $_counter'),
                    ),
                  );
                  if (result == 'You selected Disagree'){
                    _name.clear();
                    _email.clear();
                    _phone.clear();
                  };
                },
                child: Text('Go Next Page')),
          ],
        ),
      ),
    );
  }
}
